aux400 = load('../mod/rbc_periods_400_aux.mat');
noaux400 = load('../mod/rbc_periods_400.mat');
noaux8000 = load('../mod/rbc_periods_8000.mat');

figure(1)
plot(noaux8000.oo_.endo_simul(2,1:500),'-r','linewidth',2);
hold on
plot(noaux400.oo_.endo_simul(2,1:400),'-k','linewidth',2);
hold off

matlab2tikz( 'fig1.tex', 'height', '\figureheight', 'width', '\figurewidth' );

figure(2)
plot(noaux8000.oo_.endo_simul(2,1:500),'-r','linewidth',2);
hold on
plot(aux400.oo_.endo_simul(2,1:400),'-g','linewidth',2);
hold off

matlab2tikz( 'fig2.tex', 'height', '\figureheight', 'width', '\figurewidth' );

figure(3)
plot(aux400.oo_.endo_simul(2,1:400),'-g','linewidth',2);
hold on
plot(noaux400.oo_.endo_simul(2,1:400),'-k','linewidth',2);
hold off

matlab2tikz( 'fig3.tex', 'height', '\figureheight', 'width', '\figurewidth' );

system('touch fig.tex');