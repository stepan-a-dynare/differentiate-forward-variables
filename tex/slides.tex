% -*- ispell-dictionary: "american-insane" -*-

\documentclass[a4,10pt]{seminar}

\usepackage{ccicons}
\usepackage{amssymb}
\usepackage[centertags]{amsmath}
\usepackage{amstext}
\usepackage{amsbsy}
\usepackage{amsopn}
\usepackage{amscd}
\usepackage{amsxtra}
\usepackage{amsthm}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{semlayer}
\usepackage{color}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{lastpage}
\usepackage[nice]{nicefrac}
\usepackage{setspace}
\usepackage{hyperref}
\usepackage{manfnt,marvosym}

\input{seminar.bug}
\input{seminar.bg2}
\usepackage[utf8]{inputenc}

\overlaysfalse

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\pgfplotsset{plot coordinates/math parser=false}

\newlength\figureheight
\newlength\figurewidth


\newcommand{\trace}{\mathrm{tr}}
\newcommand{\vect}{\mathrm{vec}}
\newcommand{\tracarg}[1]{\mathrm{tr}\left\{#1\right\}}
\newcommand{\vectarg}[1]{\mathrm{vec}\left(#1\right)}
\newcommand{\vecth}[1]{\mathrm{vech}\left(#1\right)}
\newcommand{\iid}[2]{\mathrm{iid}\left(#1,#2\right)}
\newcommand{\normal}[2]{\mathcal N\left(#1,#2\right)}
\newcommand{\dynare}{\textsc{dynare}}
\newcommand{\sample}{\mathcal Y_T}
\newcommand{\samplet}[1]{\mathcal Y_{#1}}
\newcommand{\slidetitle}[1]{\fancyhead[L]{#1}}
\newcommand{\dates}{\texttt{@dates}\,}
\newcommand{\dseries}{\texttt{@dseries}\,}


\setlength\parindent{0pt}

\renewcommand{\headrulewidth}{0.2mm}
\renewcommand{\footrulewidth}{0.2mm}

\newenvironment{notes}{\begin{note}\setlength{\parskip}{0.1cm}\tiny\begin{spacing}{.9}}{\end{spacing}\end{note}}


\newcommand{\ladate}{February, 2014}
\newcommand{\auteur}{Stéphane Adjemian}
\newcommand{\institution}{Université du Maine, Gains}
\newcommand{\contact}{stephane.adjemian@univ-lemans.fr}
\newcommand{\titre}{differentiate\_forward\_variables}
\newcommand{\totalnumberofslides}{8}

\definecolor{gris25}{gray}{0.75}

\def\printlandscape{\special{landscape}}
\makeatletter
  \def\pst@initoverlay#1{%
  \pst@Verb{%
  /BeginOL {dup (all) eq exch TheOL le or {IfVisible not {Visible
  /IfVisible true def} if} {IfVisible {Invisible /IfVisible false
  def} if} ifelse} def \tx@InitOL /TheOL (#1) def}} \makeatother


% ----------------------------------------------------------------
% Slides *********************************************************
% ----------------------------------------------------------------

\slideframe{none}


\fancyhf{}
\fancyfoot[L]{\small \href{http://creativecommons.org/licenses/by-sa/3.0/legalcode}{\ccbysa}\hspace{.1cm}\raisebox{-.075cm}{\href{https://gitlab.com/stepan-a-dynare/differentiate-forward-variables.git}{\includegraphics[scale=.125]{../img/gitlab.png}}}}
\fancyfoot[R]{\small \ladate\ -- \theslide/\totalnumberofslides}


%\autoslidemarginstrue
\centerslidesfalse




\begin{document}

\pagestyle{fancy}



\begin{slide}
\title{\texttt{\titre}}
\author{\texttt{\auteur}\\ \texttt{\institution}\\
\textmd{\texttt{\contact}}\\}
\date{\texttt{\ladate}}
 \maketitle
\end{slide}

\begin{slide}
\slidetitle{\textsc{Perfect foresight models}}
We need to solve a system of nonlinear equations for the paths of the
endogenous variables ($y$):
\[
f(y_{t-1},y_t,y_{t+1}) = 0\quad\quad \forall t=1,\dots,T-1
\]
with an arbitrary initial condition $y_0$ (for the states) and the
terminal condition $y_T = y^{\star}$ (for the jumping variables).
\begin{itemize}
\item Main approximation: the system goes back to the
  steady state ($y^{\star}$) in finite time.
\item The system of non linear equations is solved with a
  standard Newton algorithm exploiting the sparse structure of the Jacobian.
\end{itemize}
\medskip
\textbf{Issue 1}. The steady state may be unknown.\newline
\textbf{Issue 2}. The variables may be far from the steady state at
time $T$.
\end{slide}


\begin{slide}
\slidetitle{\textsc{Terminal condition on  variations} (1)}
\begin{itemize}
\item Instead of imposing that $y$ matches the steady state at time $T$,
  we can impose that the variations of $y$ are zero at time $T$
  (assuming that there is no long term growth in the model).
\item Basically, we just need to replace any occurence of $y_{t+1}$ in
  the model by $y_t+\Delta y_{t+1}$, so that the leaded variables are
  variations instead of levels.
\item This transformation is triggered by adding an option to the
  \verb+model+ block:
\begin{verbatim}
    model(differentiate_forward_variables);
        ... EQUATIONS ... ;
    end;
\end{verbatim}
\end{itemize}
\end{slide}


\begin{slide}
\slidetitle{\textsc{Terminal condition on  variations} (2)}
\begin{itemize}
\item The preprocessor automatically  creates  one  auxiliary variable  for  each  endogenous
variable appearing with  a lead in the model.
\item If the model contains  \verb!x(1)!, then a variable  \verb!AUX_DIFF_VAR! will
be created such that \verb!AUX_DIFF_VAR=x-x(-1)!.
\item Any occurence of \verb!x(1)!  will be replaced by \verb!x+AUX_DIFF_VAR(1)!
\item By default this  transformation  is  applied  to  all  the  endogenous  variables
appearing  at time  $t+1$.
\end{itemize}
\end{slide}


\begin{slide}
\slidetitle{\textsc{Example}}
\begin{itemize}
\item Real Business Cycle model with endogenous labor supply and CES technology.
\item We calibrate a very persistent productivity ($\rho = .999$) so that in period 400 the  level of
productivity,  after an  initial one  percent shock,  is still  0,67\%
above its steady state level (1).
\item We consider three scenarii
  \begin{enumerate}
\item $T = 8000$ and a terminal condition on the levels.
\item $T = 400$ and a terminal condition on the levels.
\item $T = 400$ and a terminal condition on the variations.
  \end{enumerate}
The paths obtained under the first scenario will be interpreted as the
true paths, because it seems reasonable to assume that the economy is
back at the steady state after 8000 periods.
\end{itemize}
\end{slide}


\begin{slide}
\slidetitle{\textsc{Example, Equilibrium paths for output in the RBC model}}
\setlength\figureheight{5cm}
\setlength\figurewidth{5cm}
\begin{figure}[F]
  \centering
  \input{../img/fig1.tex}
\end{figure}
  {\small Red and black curves are respectively the solutions under scenarii 1 and 2.}

\end{slide}


\begin{slide}
\slidetitle{\textsc{Example, Equilibrium paths for output in the RBC model}}
\setlength\figureheight{5cm}
\setlength\figurewidth{5cm}
\begin{figure}[F]
  \centering
  \input{../img/fig2.tex}
\end{figure}
  {\small Red and green curves are respectively the solutions under scenarii 1 and 3.}
\end{slide}


\begin{slide}
\slidetitle{\textsc{Example, Equilibrium paths for output in the RBC model}}
\setlength\figureheight{5cm}
\setlength\figurewidth{5cm}
\begin{figure}[F]
  \centering
  \input{../img/fig3.tex}
\end{figure}
  {\small Black and green curves are respectively the solutions under scenarii 2 and 3.}
\end{slide}


\end{document}