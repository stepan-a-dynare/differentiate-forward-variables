Slides and notes about Dynare's differentiate\_forward\_variables
option.

To build note.pdf and slides.pdf you need to download matlab2tikz,
with the following instructions:

```bash
$ git submodule init
$ git submodule update
```

and to create a file called `./local.mk` where the paths to matlab,
dynare are defined. A typical `local.mk` file will look like:

```make
MATLAB = /usr/bin/matlab
DYNARE = /home/stepan/works/dynare/matlab
TIKZ = ./matlab2tikz/src
```

Once this is done you just need to type the following instruction:

```bash
$ make all
```
