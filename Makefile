ROOT_PATH = .

pdf-build:
	$(MAKE) -C $(ROOT_PATH)/tex all

note-build:
	$(MAKE) -C $(ROOT_PATH)/tex note.pdf

slides-bdf:
	$(MAKE) -C $(ROOT_PATH)/tex slides.pdf

rbc.mod:
	$(MAKE) -C $(ROOT_PATH)/mod all

figures.tikz:
	$(MAKE) -C $(ROOT_PATH)/matlab all


all: rbc.mod figures.tikz pdf-build

cleanall:
	$(MAKE) -C $(ROOT_PATH)/tex cleanall
	$(MAKE) -C $(ROOT_PATH)/mod cleanall
	$(MAKE) -C $(ROOT_PATH)/matlab cleanall

tarball:
	tar -pczf ../differentiate-forward-variables.tar.gz . --exclude .git --exclude matlab/matlab2tikz/.git
